<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CheckPermissionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next, $permissionName)
    {
        $user = auth()->user();
        if ($user->hasPermission($permissionName) || $user->isSuperAdmin())
        {
            return $next($request);
        }
        return abort(Response::HTTP_FORBIDDEN, 'This action is unauthorized.');
    }
}
