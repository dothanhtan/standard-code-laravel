@extends('layouts.dashboard')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col col-md-12">
                @if(session('message'))
                    <div class="alert alert-success">
                        <strong>{{session('message')}}</strong>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card card-plain">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title text-center mt-0">USERS LIST</h4>
                    </div>
                    <div class="card-body">
                        <div class="d-flex justify-content-between">
                            <div>
                                <a href="{{route('users.create')}}" class="btn btn-sm btn-primary my-3"><i class="fas fa-plus"></i> ADD</a>
                            </div>

                            <form class="mt-3">
                                <div class="input-group mb-3">
                                    <input type="text" value="" class="form-control" placeholder="Name...">
                                    <div class="input-group-append">
                                        <button class="btn btn-sm btn-info" type="button"><i class="fas fa-search"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead class="">
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Roles</th>
                                    <th>Action</th>
                                </thead>
                                @foreach($users as $user)
                                <tbody>
                                    <tr>
                                        <td>
                                            {{$user->id}}
                                        </td>
                                        <td>
                                            {{$user->name}}
                                        </td>
                                        <td>
                                            {{$user->email}}
                                        </td>
                                        <td>
                                            @foreach($user->roles as $role)
                                                <span class="badge rounded-pill bg-info text-dark">{{$role->name}}</span>
                                            @endforeach
                                        </td>
                                        <td class="d-flex">
                                            @hasRole('manager')
                                            <div class="mr-2">
                                                <a href="{{route('users.show', $user->id)}}" class="btn btn-info btn-sm"><i class="fas fa-eye"></i></a>
                                            </div>
                                            @endhasRole

                                            @hasRole('manager')
                                            <div class="mr-2">
                                                <a href="{{route('users.edit', $user->id)}}" class="btn btn-warning btn-sm"><i class="fas fa-pencil"></i></a>
                                            </div>
                                            @endhasRole

                                            <form id="deleteForm{{$user->id}}" action="{{route('users.destroy', $user->id)}}" method="post">
                                                @csrf
                                                @method('DELETE')
                                            </form>
                                            @hasRole('manager')
                                            <button data-form="deleteForm{{$user->id}}" class="btn btn-delete btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                                            @endhasRole
                                        </td>
                                    </tr>
                                </tbody>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{$users->links()}}
    </div>
@endsection

@section('script')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script>
        $(function (){
            $(document).on('click', '.btn-delete', function () {
                let formID = $(this).data('form')
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $(`#${formID}`).submit()
                        Swal.fire(
                            'Deleted!',
                            'This user has been deleted.',
                            'success'
                        )
                    }
                })
            })
        })
    </script>
@endsection
