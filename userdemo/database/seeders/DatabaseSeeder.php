<?php

namespace Database\Seeders;

use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $roleAdmin = Role::updateOrCreate(['name' => 'admin'], ['display_name' => 'Quản trị viên']);
        $roleUser = Role::updateOrCreate(['name' => 'user'], ['display_name' => 'Khách hàng']);
        $roleManager = Role::updateOrCreate(['name' => 'manager'], ['display_name' => 'Người quản lý']);

        $permissionShowUser = Permission::updateOrCreate(['name' => 'show-user'], ['display_name' => 'Xem người dùng']);
        $permissionCreateUser = Permission::updateOrCreate(['name' => 'create-user'], ['display_name' => 'Tạo người dùng']);
        $permissionUpdateUser = Permission::updateOrCreate(['name' => 'update-user'], ['display_name' => 'Cập nhật người dùng']);
        $permissionDeleteUser = Permission::updateOrCreate(['name' => 'delete-user'], ['display_name' => 'Xóa người dùng']);

        $roleManager->permissions()->attach([$permissionShowUser->id, $permissionCreateUser->id, $permissionUpdateUser->id, $permissionDeleteUser->id]);

        $admin = User::whereEmail('admin@gmail.com')->first();

        if (!$admin)
        {
            $admin = User::factory()->create(['email' => 'admin@gmail.com']);
        }
        $admin->roles()->sync($roleAdmin->id);
    }
}
