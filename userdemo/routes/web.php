<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');

Route::middleware(['auth'])->name('users.')->prefix('users')->controller(UserController::class)->group(function () {
    Route::get('/','index')->name('index');

    Route::get('/create','create')->name('create')->middleware('check.permission:create-user');

    Route::post('/store','store')->name('store');

    Route::get('/edit/{id}','edit')->name('edit')->middleware('check.permission:update-user');

    Route::put('/update/{id}','update')->name('update');

    Route::get('/show/{id}','show')->name('show')->middleware('check.permission:show-user');

    Route::delete('/destroy/{id}','destroy')->name('destroy')->middleware('check.permission:delete-user');
});
